# Terraform

In this ReadMe file you will find information about Terraform. You will find a detailed description of how to install
Terraform and information about important terms and elements such as the Terraform lifecycle.

## Terraform Lifecycle

The Terraform life cycle consists of the following elements: init, plan, apply and destroy.

```
terraform init
```

Initialize the project and the local Terraform environment. It also downloads all necessary providers.

```
terraform plan
```

This command creates an execution plan for you and shows the user a preview of all changes that Terraform will implement
in the infrastructure. It is important to note that no changes are made with this command, the command is only used to
see whether the proposed changes correspond to the desired ones.
If the terraform plan command does not detect any changes, it reports that no action is required.

`terraform plan` can be used with various options. One of them is the ability to save the generated plan as a file. This
can then be used with the `terraform apply` command. For example, to save the plan with the name "myplan", you execute
the following command: `terraform plan -out=myplan`.

```
terraform apply
```

The terraform apply command basically executes the suggested changes that were displayed with the terraform plan
command.
There are two modes for this command: Automatic Plan Mode and Saved Plan Mode.

**Automatic Plan Mode**

When terraform apply is executed and no saved plan file is provided with the command, Terraform will automatically
create a new execution plan (as if terraform plan had been executed previously). Subsequently, you will receive a prompt
to confirm or reject the changes.

To skip this prompt, you can execute `terraform apply -auto-approve`. This will result in the changes being
automatically applied

**Saved Plan Mode**

In Saved Plan Mode, the saved plan file can be passed. Terraform will execute the changes according to the saved plan
file without requiring confirmation from the user. The command to use the Saved Plan Mode would look like this:
`terraform apply myplan`

It's important to note that when using Saved Plan Mode, no additional modes or options can be used when running
terraform apply.

```
terraform destroy
```

The command `terraform destroy`, as the name suggests, deletes all resources defined in the Terraform configuration. In
our project, this would mean deleting the Azure configurations including databases and containers.

If you want to delete a specific resource, you can do so using the
command `terraform destroy -target="ressource.ressource_name"` for example `terraform destroy -target="azurerm_container_group.container"`.

## Installation

### Prerequisites

- Make sure that you have downloaded
  the [Terraform binary package](https://developer.hashicorp.com/terraform/install#windows) distributed by HashiCorp.
- Make sure that Docker is installed

### Information at the start

A Windows 11 system was used for this guide. The steps/commands may differ for other systems such as Mac OS or Linux.

The downloaded folder of Terraform must then be unzipped into a folder of your choice, for example under "C:\terraform".
It is important that you remember the storage location, because you have to adjust the environment variable "PATH" and
add the directory (in this case "C:\terraform") to the variable "PATH".

### Change PATH and verify installation

To make changes to the PATH variable, you can enter "env" (environment variables) directly in the search and then open
it.

The system properties are opened, and you will find a button called "Environment variables" at the bottom right which
you click on.

![System properties](images/systemProperties.png)

Select the variable "Path" and click on "Edit...".

![Environment variables](images/environmentVariables.png)

Then click on "New" and add the path where you previously unzipped the downloaded folder. In this case "C:\terraform".

![Add path to variable PATH](images/addPathtoPATH.png)

To verify the installation, open a new Powershell console and enter the following command:

```shell
terraform -help
```

You get an overview of all possible commands for Terraform. Terraform was successfully installed.

![Terraform after installation](images/powerShellAfterInstallation.png)

If an error message such as "Terraform could not be found" appears, the PATH variable may not have been set
correctly. If this is the case, check again whether the PATH variable contains the correct directory.

## Important documents

There are several documents which are important for the terraform configuration. In this part, we will go through every
document and explain how the code works, and what you need to do, to redo our project.

### main.tf

The file [main.tf](../../main.tf) is used to create the Azure infrastructure. It is also responsible for providing an
SQL server, two databases and the containers for the various applications (Bank API, Accounting API etc.)

The provider has been defined in the following code snippet. Azure is used in this project and defined accordingly.
Our backend is configured with GitLab. Read more about the setup on GitLab in our [setup guide](../setup/README.md).

```terraform
terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "~> 3.0.2"
    }
  }
  backend "http" {
  }
}
```

The following code snippet defines the resource group on Azure:

```terraform
resource "azurerm_resource_group" "rg" {
  name     = "Terraform-${var.branch}"
  location = var.location
}
```

`"azurerm_resource_group" "rg"` "azurerm_resource_group" describes the type of resource.
In this case, an Azure resource group. "rg" stands for the alias of the resource and must be unique.
In our configuration, the name and location of the resource group is defined with variables. These are located in the
variables.tf file

We also create the following resources in our configuration:

- `azurerm_mssql_server` An SQL Server
- `azurerm_mssql_firewall_rule` Firewall rules for the SQL Server. Here we make sure that all Azure services can access
  the server.
- `azurerm_mssql_database` Two SQL databases (accounting_db & client_db)

The main.tf file also contains the configurations for the containers inside the Azure container group.
These all follow the same pattern. The following code snippet shows the configuration for the Bank API:

```terraform
  container {
  name   = "bank-api"
  image  = "registry.gitlab.com/ivan.kostanje/gitops-and-iac/bank-api:${var.branch}"
  cpu    = var.cpu
  memory = var.memory

  ports {
    port     = 8080
    protocol = "TCP"
  }
}
```

- `name`: The name of the container is defined, in this case it is for the bank API, so the name is bank-api
- `image`: Address to the correct Docker image is defined here.
- `cpu` and `memory`: Settings for memory resources, which are defined via variables.
- `ports`: Specifies the port via which the container communicates, in this case 8080.

Such a container was created for each component of our project. This includes:

- ahv-api
- bank-api
- accounting-api
- safetynet-api
- frontend

### outputs.tf

The IP address of the container is returned in the file [output.tf](../../outputs.tf). This value will be shown in
the console after the build finishes. It can also be displayed with the `terraform ouput` command.

```terraform
output "container_ipv4_address" {
  value = azurerm_container_group.container.ip_address
}
```

### variables.tf

The [variables.tf](../../variables.tf) file contains the defined variables which were used in the main.tf file, such as
the variables "location", "cpu" or "memory".

If you define a variable as *sensitive*, terraform will ask you, when creating the plan to input the values in the
terminal. Or you can set them in environment variables with the syntax `TF_VAR_<VARIABLE_NAME>`. This is very useful
for values such as API keys, passwords, and usernames.

```terraform
variable "location" {
  description = "The location/region where the Azure Resource Group should be created."
  default     = "switzerlandnorth"
}

variable "db_admin_username" {
  description = "Admin username of the entire SQL server"
  sensitive = true
}
```

### .gitlab-ci.yml

The .gitlab-ci.yml file is not a Terraform file. But it is very important in this project. As this is the configuration
file for the pipelines and is used to automate the CI/CD process.
Terraform is used in the stage "deploy", to deploy the applications in Azure.

The following code snippet is only executed if the current branch is not the default branch (main) and the CI/CD
pipeline was triggered on the web.
Terraform commands are then also found under "script:". `terraform init` to establish the connection to GitLab and
download the providers, and `terraform apply` to apply the changes to the infrastructure.
It is important to note that `-auto-approve` determines that the changes are executed without manual confirmation.

```yml
deploy-dev-job:
  stage: deploy
  rules:
    - if: ($CI_COMMIT_BRANCH != $CI_DEFAULT_BRANCH && $CI_PIPELINE_SOURCE == "web")
  image: 
    name: hashicorp/terraform:1.7.4
    entrypoint: [""]
  script:
    - export TF_VAR_db_admin_username=$DB_USER TF_VAR_db_admin_password=$DB_PASS
    - terraform init -backend-config="address=$ADDRESS" -backend-config="lock_address=$LOCK_ADDRESS" -backend-config="unlock_address=$UNLOCK_ADDRESS" -backend-config="username=$USERNAME" -backend-config="password=$GITLAB_ACCESS_TOKEN" -backend-config="lock_method=POST" -backend-config="unlock_method=DELETE" -backend-config="retry_wait_min=5"
    - terraform apply -replace="azurerm_container_group.container" -var branch="$CI_COMMIT_BRANCH" -auto-approve

```

## Terraform components

### Provider

A provider in Terraform is a plugin that allows the user to interact with an API or a service.
Azure was used as the provider in this project. But Terraform is very versatile and there are
[many different providers](https://registry.terraform.io/browse/providers) for different use cases. This makes it
also very easy to exchange the deployment platform if needed. You would just need to check the provider documentation
of the new service and adapt your infrastructure code to be compatible with it.

### Resources

Resources are components in a terraform configuration. Examples include virtual machines, SQL databases, resource
groups, etc. These infrastructure objects can then be managed via Terraform. This means that everything deployed
with Terraform is a resource.

### Variables

In Terraform, we have input and output variables. The different variables can be used in other configuration files.

### State

Terraform states are files in which information about the infrastructure is stored. These files enable Terraform to find
and manage the resource. They should be kept privat, as they contain sensitive information.

## Terraform commands

An overview of various Terraform commands. **Please note:** The list is not complete and there are a lot of more
commands to use.

- ``terraform -version``: shows the installed Terraform version
- ``terraform init``: Initialize a working directory
- ``terraform init -input=true``: Ask for input if necessary
- ``terraform plan``: Creates execution plan
- ``terraform plan -out=path``: Saving the generated plan as a file
- ``terraform apply``: Executing the changes on your infrastructure (manual confirmation required)
- ``terraform apply -auto-approve``: Executing the changes on your infrastructure (no manual confirmation required)
- ``terraform destroy``: Delete resources (manual confirmation required)
- ``terraform validate``: Check the syntax in the configuration files

## Conclusion

This guide gives you a basic understanding of Terraform. About how Terraform can be used to configure your
infrastructure, what the Terraform lifecycle is and about the various configuration options. Terraform is a powerful
tool in the area of Infrastructure as a Code.
