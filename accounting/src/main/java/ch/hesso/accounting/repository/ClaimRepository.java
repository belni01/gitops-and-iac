package ch.hesso.accounting.repository;

import ch.hesso.accounting.model.Claim;
import org.springframework.data.repository.CrudRepository;

/**
 * @author Ivan Kostanjevec
 */
public interface ClaimRepository extends CrudRepository<Claim, Integer> {
}
