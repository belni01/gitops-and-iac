package ch.hesso.accounting.repository;

import ch.hesso.accounting.model.Claim;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * @author Ivan Kostanjevec
 */
@Repository
public class AccountingRepository {

    private final ClaimRepository claimRepository;

    @Autowired
    public AccountingRepository(ClaimRepository claimRepository) {
        this.claimRepository = claimRepository;
    }

    public boolean saveClaim(Claim claim) {
        try {
            claimRepository.save(claim);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
