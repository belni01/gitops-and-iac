package ch.hesso.accounting.model;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import lombok.*;

/**
 * @author Ivan Kostanjevec
 */
@Entity
@Getter
@Setter
@NoArgsConstructor
@RequiredArgsConstructor
public class Claim {

    @Id
    @GeneratedValue
    private int id;
    @NonNull
    private int amount;
    @NonNull
    private String iban;

    @Override
    public String toString() {
        return "{\"amount\":" + amount + ",\"iban\":\"" + iban + "\"}";
    }
}
