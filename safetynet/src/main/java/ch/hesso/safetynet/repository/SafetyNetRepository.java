package ch.hesso.safetynet.repository;

import ch.hesso.safetynet.model.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * @author Ivan Kostanjevec
 */
@Repository
public class SafetyNetRepository {

    private final CustomerRepository repository;

    @Autowired
    public SafetyNetRepository(CustomerRepository repository) {
        this.repository = repository;
    }

    public void saveCustomer(Customer customer) {
        Customer customerDB = repository.findCustomerByAhvNumber(customer.getAhvNumber());
        if (customerDB == null) repository.save(customer);
    }
}
