package ch.hesso.safetynet.model;

/**
 * @author Ivan Kostanjevec
 */
public record AhvNumber(String ahvNumber) {

    @Override
    public String toString() {
        return "{\"ahvNumber\":\"" + ahvNumber + "\"}";
    }
}
