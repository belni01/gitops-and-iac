package ch.hesso.safetynet.model;

import jakarta.persistence.*;
import lombok.*;

/**
 * @author Ivan Kostanjevec
 */
@Entity
@Getter
@Setter
@NoArgsConstructor
@RequiredArgsConstructor
public class Customer {

    @Id
    @GeneratedValue()
    private int id;
    @NonNull
    private String firstname;
    @NonNull
    private String lastname;
    @NonNull
    private String iban;
    @NonNull
    private String ahvNumber;
}
