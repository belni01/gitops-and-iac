﻿using BankAPI.Controllers;
using BankAPI.Models;
using JetBrains.Annotations;
using Microsoft.AspNetCore.Mvc;
using Xunit;

namespace BankAPI.UnitTest.Controllers;

[TestSubject(typeof(BankController))]
public class BankControllerTest
{
    [Fact]
    public void Post_WithNegativeAmount_ReturnsBadRequest()
    {
        // Arrange
        var controller = new BankController();
        var request = new TransferRequest { Amount = -1, IBAN = "NL01BANK1234567890" };

        // Act
        var result = controller.Post(request);

        // Assert
        Assert.IsType<BadRequestObjectResult>(result);
    }

    [Fact]
    public void Post_WithEmptyIBAN_ReturnsBadRequest()
    {
        // Arrange
        var controller = new BankController();
        var request = new TransferRequest { Amount = 1, IBAN = "" };

        // Act
        var result = controller.Post(request);

        // Assert
        Assert.IsType<BadRequestObjectResult>(result);
    }

    [Fact]
    public void Post_WithValidRequest_ReturnsOk()
    {
        // Arrange
        var controller = new BankController();
        var request = new TransferRequest { Amount = 1, IBAN = "NL01BANK1234567890" };

        // Act
        var result = controller.Post(request);

        // Assert
        Assert.IsType<OkResult>(result);
    }
}